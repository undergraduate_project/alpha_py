import alpha, json, sys, math, time
import os 


DIR_PATH = os.path.dirname(os.path.realpath(__file__))
PARAM = DIR_PATH + "/../../params/PID_gz.json"

LOG_PATH = DIR_PATH + "/../../logs/identification_logs"
 
CONTROLLER_PARAMS = {}

DELAY = 2


def set_raw(vehicle):
    vehicle.manual()
    time.sleep(DELAY)
    vehicle.set_control_type('raw')
    vehicle.drive()
    time.sleep(DELAY)


def set_cotroller(vehicle): 
    vehicle.manual()
    time.sleep(DELAY)
    vehicle.set_control_type("controller")
    vehicle.drive()
    time.sleep(DELAY)

    vehicle.set_controller("SwPid", dict())
    
    if (vehicle.get_mode() != 2):
        print("drive is not set, retry")
        time.sleep(DELAY)
        set_cotroller(vehicle)


def sign(value):
    return 1 if value >= 0 else -1


if __name__ == "__main__":

    # setup vehicle
    with open(PARAM) as param_file:
        vehicle_params = alpha.VehicleParams(json.load(param_file))

    CONTROLLER_PARAMS = vehicle_params['controller_params'] 

    AWSCfg = alpha.WSCfg()

    vehicle = alpha.Vehicle(vehicle_params, AWSCfg)
    vehicle.stop_vehicle_logger()
    vehicle.change_vehicle_logger_dir(LOG_PATH)
    vehicle.drive()
    time.sleep(DELAY)


    max_car_angle = 600

    torques = [] 
    for i in  range(-400, 0, 50):
        torques.append(i)

    print("Identification on this torques: ")
    print(torques)


    for torque in torques:

        set_cotroller(vehicle)
        vehicle.steer(angle=0)
        try:
            input("Set angle " + str(0) + " and press Enter "
                  "to start identification on torque " + str(torque)) 
        except KeyboardInterrupt:

            print("skip torque " + str(torque))
            vehicle.start_vehicle_logger()
            time.sleep(DELAY)
            vehicle.stop_vehicle_logger(str(torque))
            break


        # start identification on current torque 
        set_raw(vehicle)

        vehicle.start_vehicle_logger()

        time.sleep(0.1)

        print(">>> start identification on torque " + str(torque)) 

        stop_angle = sign(torque) * max_car_angle * 0.8

        starttime = time.time()
        try:
            while (abs(vehicle.get_steering_wheel_angle()) < abs(stop_angle)
                    and (time.time() - starttime) < 8):
                vehicle.steer(torque=(torque / 10.))
                
        except KeyboardInterrupt:
            print("skip torque " + str(torque))


        vehicle.stop_vehicle_logger(str(torque))
        vehicle.steer(torque=0)
        print("<<< stop identification on torque " + str(torque)) 

    print("Identification done!") 
