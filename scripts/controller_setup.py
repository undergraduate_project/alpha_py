import alpha, json, sys, math, time, os

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

def sin(veh, max, freq):
    try:
         while True:
              veh.steer(max * math.sin(freq * time.time()))

    except KeyboardInterrupt:
         pass
         

if __name__ == "__main__":
        controller = input("Controller name (PID_gz/LUT_gz/LUTSmith_gz) \n")
        param      = DIR_PATH + "/../params/" + controller + ".json"

        with open(param) as param_file:
            vehicle_params = alpha.VehicleParams(json.load(param_file))

        AWSCfg = alpha.WSCfg()

        vehicle = alpha.Vehicle(vehicle_params, AWSCfg)
        vehicle.drive()

