### Генерация документации

1. Установите sphinx версии 2.8 или выше:

    ```
    apt install python3-sphinx
    ```

    или

    ```
    pip3 install --user sphinx
    ```

2. Из директории docs выполните

    ```
    make html
    ```

3. Откройте в браузере docs/build/html/index.html
