.. Alpha documentation master file, created by
   sphinx-quickstart on Fri Jan 28 16:00:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Alpha
=============================================

Python API для низкоуровневой системы управления беспилотных транспортных средств `Alpha <https://alpha.starline.ru//>`_,
разрабатываемой в рамках проекта `OSCAR <https://gitlab.com/starline/oscar/>`_.


.. include:: install.rst

.. include:: howto.rst


API
------------------
.. automodule:: alpha.alpha
   :members:

.. autoclass:: alpha.protocol.VEHICLE_MODE
   :members:


Controller
------------------
.. automodule:: alpha.control
  :members:
