Пример использования
------------------------------

::

    import alpha
    vehicle = alpha.Vehicle(“/dev/ttyACM0”)
    vehicle.drive()
    vehicle.steer(20)
    vehicle.move(10)
    vehicle.manual()
    vehicle.led_blink()
    vehicle.emergency_stop()
    vehicle.recover()
    vehicle.left_turn_signal()
    vehicle.right_turn_signal()
    vehicle.emergency_signals()
    vehicle.turn_off_signals()
    vehicle.get_vehicle_speed()
