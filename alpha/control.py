#!/usr/bin/env python

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import inspect
import sys
import time
import threading

from math import cos, sin, tan, copysign, exp, inf

from . import log


def available_controllers():
    """
    Возвращает словарь с доступными контроллерами
    Ключ - наименование контроллера
    Значение - соответствующий класс контроллера

    :rtype: dict
    """

    controllers = {}
    for name, class_type in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(class_type) and name != "BaseController":
            controllers.update({name: class_type})

    return controllers


def new_controller(controller_name, vehicle, params = None):
    """
    Возвращает объект контроллера для автомобиля или None в случае
    невозможности найти контроллер с заданным наименованием

    :param controller_name: наименование контроллера
    :type controller_name: str
    :param vehicle: объект автомобиля, используется для получения актуальных
    показаний с автомобиля для реализации управления
    :type vehicle: Vehicle
    :param params: параметры контроллера
    :type params: dict
    """

    if controller_name:
        controllers = available_controllers()
        if controller_name in controllers:
            return controllers[controller_name](vehicle, params)
        else:
            log.print_warn("There is no " + str(controller_name) + " controller!")

    return None


class BaseController(object):
    """
    Базовый клас контроллера. Не реализует управления и служит для основы для
    создания всех контроллеров, работающих в рамках API. Подкласс Params определяет
    параметры для контроллера по умолчанию.

    :param vehicle: объект автомобиля, используется для получения актуальных
    показаний с автомобиля для реализации управления
    :type vehicle: Vehicle
    :param params: параметры контроллера
    :type params: BaseController.Params
    """

    MAX_DELAY_IN_CONTROL_LOOP = 0.2

    class Params(dict):

        def __init__(self, *args, **kwargs):

            super(BaseController.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        self._params = self.Params(params) if params else self.Params()

        self._vehicle = vehicle

        self._state_lock = threading.Lock()

        self._target_vehicle_speed        = None
        self._target_vehicle_acceleration = None
        self._target_vehicle_jerk         = None
        self._target_vehicle_throttle     = None

        self._target_sw_angle    = None
        self._target_sw_velocity = None
        self._target_sw_torque   = None

        self._output_vehicle_throttle = None
        self._output_sw_torque        = None


    def update_params(self, params):
        self._params.update(params)


    def get_params(self):
        return self._params


    def reset(self):
        pass


    def get_target_steering(self):
        return self._target_sw_angle


    def set_target_move(self, speed        = None,
                              acceleration = None,
                              jerk         = None,
                              throttle     = None):

        self._target_vehicle_speed        = speed
        self._target_vehicle_acceleration = acceleration
        self._target_vehicle_jerk         = jerk
        self._target_vehicle_throttle     = throttle


    def set_target_steering(self, angle    = None,
                                  velocity = None,
                                  torque   = None):

        self._target_sw_angle    = angle 
        self._target_sw_velocity = velocity
        self._target_sw_torque   = torque


    def _calc_throttle_output(self, d_time):
        self._output_vehicle_throttle = clamp(-100, self._target_vehicle_throttle, 100)

    
    def _calc_sw_torque_output(self, d_time):
        pass


    def calc_output(self):
        cur_time = time.time()
        d_time = cur_time - self._last_control_time
        self._last_control_time = cur_time

        if (d_time > self.MAX_DELAY_IN_CONTROL_LOOP):
            # log.print_warn("[ALPHA]: Control loop rate is too low")
            print("[ALPHA]: Control loop rate is too low")
            # d_time = self.MAX_DELAY_IN_CONTROL_LOOP
            # return self._output_vehicle_throttle, self._output_sw_torque

        self._calc_throttle_output(d_time)
        self._calc_sw_torque_output(d_time)

        return self._output_vehicle_throttle, self._output_sw_torque


class SwPid(BaseController):

    class Params(dict):

        def __init__(self, *args, **kwargs):

            self["P"]              = 0.1
            self["I"]              = 0.1
            self["I_saturation"]   = 80.
            self["D"]              = 0.001
            self["out_saturation"] = 90.

            super(SwPid.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPid, self).__init__(vehicle, params)

        self._target_sw_angle    = 0.0
        self._target_sw_velocity = 0.0

        self._target_vehicle_throttle = 0.0

        self._output_sw_torque = 0.0

        self._last_sw_angle_error = 0.0
        self._last_control_time   = 0.0

        self._i_term = 0.0


    def reset(self):
        self._last_control_time = time.time()
        self._target_sw_angle   = self._vehicle.get_steering_wheel_angle()
        self._last_sw_angle_error = 0.0
        self._i_term = 0.0


    def _calc_sw_torque_output(self, d_time):

        # cur_vehicle_speed = self._vehicle.get_vehicle_speed()
        cur_sw_angle = self._vehicle.get_steering_wheel_angle()

        sw_angle_error = self._target_sw_angle - cur_sw_angle

        p_term = sw_angle_error * self._params['P']

        new_i_term = self._i_term + sw_angle_error * d_time * self._params['I']
        if abs(new_i_term) <  self._params['I_saturation']:
            self._i_term = new_i_term

        d_sw_angle_error = sw_angle_error - self._last_sw_angle_error
        self._last_sw_angle_error = sw_angle_error

        d_term = (d_sw_angle_error / d_time) * self._params['D']

        output_sw_torque = p_term + self._i_term + d_term

        if abs(output_sw_torque) <  self._params['out_saturation']:
            self._output_sw_torque = output_sw_torque
        else:
            self._output_sw_torque = copysign(self._params['out_saturation'], output_sw_torque)


class SwPidVelRanges(SwPid):

    class Params(SwPid.Params):

        def __init__(self, *args, **kwargs):

            self['vel_ranges'] = []
            self['vel_step']   = 1.0

            super(SwPidVelRanges.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPidVelRanges, self).__init__(vehicle, params)

        self._vrange = None
        self._vranges = []

        self._rparams = {}

        self._update_params_l = threading.Lock()

        self._last_velocity = self._vehicle.get_vehicle_speed()

        self._check_vranges()


    def reset(self):
        super(SwPidVelRanges, self).reset()
        self._vrange = None


    def update_params(self, params):

        with self._update_params_l:

            super(SwPidVelRanges, self).update_params(params)
            self._check_vranges()


    def _pick_vrange(self):

        for vrange in self._vranges:

            lower = vrange['lower'] if 'lower' in vrange else -inf
            upper = vrange['upper'] if 'upper' in vrange else  inf

            if lower <= self._last_velocity and self._last_velocity <= upper:

                self._rparams = vrange
                return

        # -------------------------------------------------

        self._rparams = self._params


    def _check_vranges(self):

        self._vranges = []

        if not 'vel_ranges' in self._params:
            return

        # -------------------------------------------------

        for vrange in self._params['vel_ranges']:

            cur_lower = vrange['lower'] if 'lower' in vrange else -inf
            cur_upper = vrange['upper'] if 'upper' in vrange else  inf

            if cur_lower > cur_upper:

                print('[ALPHA]: Controller vel range lower is bigger ' +
                      'then upper! Default main params is used.')

                return

            # comparison to neighbor ranges ---------------

            for nhbr_vrange in self._params['vel_ranges']:

                if nhbr_vrange is vrange:
                    continue

                nhbr_lower = nhbr_vrange['lower'] if 'lower' in vrange else -inf
                nhbr_upper = nhbr_vrange['upper'] if 'upper' in vrange else  inf

                if (cur_lower > nhbr_lower and cur_upper < nhbr_upper or
                    cur_lower == -inf and nhbr_lower == -inf          or
                    cur_upper ==  inf and nhbr_upper ==  inf):

                    print('[ALPHA]: Controller vel ranges has ' +
                        'intersections! Default main params is used.')

                    return

        self._vranges = self._params['vel_ranges']
        self._pick_vrange()


    def _calc_sw_torque_output(self, d_time):

        cur_velocity = self._vehicle.get_vehicle_speed()
        cur_sw_angle = self._vehicle.get_steering_wheel_angle()

        if abs(self._last_velocity - cur_velocity) > self._params['vel_step']:

            self._last_velocity = cur_velocity

            with self._update_params_l:
                self._pick_vrange()

        sw_angle_error = self._target_sw_angle - cur_sw_angle

        p_term = sw_angle_error * self._rparams['P']

        new_i_term = self._i_term + sw_angle_error * d_time * self._rparams['I']
        if abs(new_i_term) <  self._rparams['I_saturation']:
            self._i_term = new_i_term

        d_sw_angle_error = sw_angle_error - self._last_sw_angle_error
        self._last_sw_angle_error = sw_angle_error

        d_term = (d_sw_angle_error / d_time) * self._rparams['D']

        output_sw_torque = p_term + self._i_term + d_term

        if abs(output_sw_torque) <  self._rparams['out_saturation']:
            self._output_sw_torque = output_sw_torque
        else:
            self._output_sw_torque = copysign(self._rparams['out_saturation'], output_sw_torque)


class LUT:

    def  __init__(self) -> None:
        self._max_key = 0
        self._min_key = 0

        self._data: dict = None


    def show(self):

        print("{:<20} |{:<20}".format('Key','Value'))
        print("-----------------------------------")

        for k, v in self._data.items():
            print("{:<20} |{:<20}".format(k, v))


    def load(self, file_path):

        data = dict()
        
        with open(file_path, 'r') as file:
            
            for line in file:
                key, value = line.strip().split(' ', 1)
                data.update({float(key) : float(value)})

        self.set_data(data)


    def set_data(self, data: dict):

        self._data = dict(sorted(data.items(), key=lambda item: item[0]))
        
        self._min_key = next(iter(self._data.keys())) 
        self._max_key = next(reversed(self._data.keys()))  
            
    
    def reverse_cols(self):

        if not self._data:
            raise Exception("[LUT] Load data before reverse")
        
        reversed = {val: key for key, val in self._data.items()}
        
        self.set_data(reversed)


    def get_value(self, in_key):

        if not self._data:
            raise Exception("[LUT] Load data before get value")

        in_key = clamp(self._min_key, in_key, self._max_key)

        if in_key in (self._min_key, self._max_key):
            return self._data[in_key]
        
        # if value is LUT

        lower_key, upper_key = None, None
        keys = list(self._data.keys())
        for idx, key in enumerate(keys):
            if in_key <= key:

                lower_key = keys[idx - 1]
                upper_key = keys[idx]
                
                break

        return self.get_interpolated(in_key, lower_key, upper_key,
                                self._data[lower_key], self._data[upper_key] )


    def get_interpolated(self,   x_val, 
                         x_lower, x_upper, 
                         y_lower, y_upper):
        return y_lower + (y_upper - y_lower) * (x_val - x_lower) / (x_upper - x_lower)


class SmithPredictor():
    def __init__(self, model, delay: float):
        self._model = model
        self._delay = delay  # seconds

        self._history = []


    def priedict_value(self, input_value, current_system_output, dtime):

        predicted_output = self._model(input_value, current_system_output, dtime)

        self._history.append((time.time(), predicted_output))

        if self._is_has_delayed():
            print(predicted_output - self._history[0][0])
            return predicted_output - self._history.pop(0)[1]

        return predicted_output

    
    def _is_has_delayed(self):
        if len(self._history) == 0:
            return False

        return time.time() - self._history[0][0] >= self._delay


# PI + Table + Smith
class CustromСontroller(SwPid):
    class Params(dict):

        def __init__(self, *args, **kwargs):

            self["P"]              = 0.1
            self["I"]              = 0.1
            self["I_saturation"]   = 80.
            self["D"]              = 0.0
            self["out_saturation"] = 90.

            self["LUT_path"] = ""

            self["system_delay"]   = 0.0  # [seconds] # if value == 0 Smith predictor is off
            self["smith_saturation"] = 90.

            super(CustromСontroller.Params, self).__init__(*args, **kwargs)

    
    def __init__(self, vehicle, params = None):
        super(CustromСontroller, self).__init__(vehicle, params)

        self._lut = LUT()
        self._lut.load(self._params["LUT_path"])

        self._predictor = SmithPredictor(self._sw_model, self._params["system_delay"])

        self._target_sw_angle    = 0.0
        self._output_sw_torque = 0.0

        self._last_sw_angle_error = 0.0
        self._i_term = 0.0

        self._last_predicted = 0.0

        #debug
        self.debug_pid = 0
        self.debug_smith = 0
        self.debug_lut = 0
        self.debug_out = 0


    def _sw_model(self, controller_out, system_output, dtime):
        return system_output + dtime * controller_out  # integrator


    def reset(self):
        self._last_control_time = time.time()
        self._target_sw_angle   = self._vehicle.get_steering_wheel_angle()
        self._last_sw_angle_error = 0.0
        self._i_term = 0.0
        self._last_predicted = 0.0


    def _calc_pid(self, error, dtime):

        p_term = error * self._params['P']

        new_i_term = self._i_term + error * dtime * self._params['I']
        if abs(new_i_term) <  self._params['I_saturation']:
            self._i_term = new_i_term

        d_sw_angle_error = error - self._last_sw_angle_error
        self._last_sw_angle_error = error

        d_term = (d_sw_angle_error / dtime) * self._params['D']

        return p_term + self._i_term + d_term


    def _calc_sw_torque_output(self, d_time):

        cur_vehicle_speed = self._vehicle.get_vehicle_speed()
        cur_sw_angle = self._vehicle.get_steering_wheel_angle()

        sw_angle_error = self._target_sw_angle  - cur_sw_angle + self._last_predicted

        torque = self._calc_pid(sw_angle_error, d_time)
        self.debug_pid = torque

        self._last_predicted = self._predictor.priedict_value(torque, cur_sw_angle, d_time)        
        self.debug_smith = self._last_predicted

        sw_angular_vel = self._lut.get_value(torque) / 10
        self.debug_lut = sw_angular_vel

        self._output_sw_torque = sw_angular_vel
        self.debug_out = self._output_sw_torque

        #if abs(torque) <  self._params['out_saturation']:
        #    self._output_sw_torque = torque
        #else:
        #    self._output_sw_torque = copysign(self._params['out_saturation'], torque)


class SwPidInEWMA(SwPid):

    class Params(SwPid.Params):

        def __init__(self, *args, **kwargs):

            self["frequency"] = 10

            super(SwPidInEWMA.Params, self).__init__(*args, **kwargs)


    def __init__(self, vehicle, params = None):

        super(SwPidInEWMA, self).__init__(vehicle, params)
        self._last_EWMA = 0.0


    def reset(self):
        super(SwPidInEWMA, self).reset()
        self._last_EWMA = self._target_sw_angle


    def set_target_steering(self, angle    = None,
                                  velocity = None,
                                  torque   = None):

        cur_time = time.time()
        d_time = cur_time - self._last_control_time
        self._last_control_time = cur_time

        alpha = 1 - exp((-1.0 * d_time) * self._params["frequency"])

        EWMA = alpha * angle + (1 - alpha) * self._last_EWMA

        self._last_EWMA = self._target_sw_angle

        self._target_sw_angle    = EWMA
        self._target_sw_velocity = velocity
        self._target_sw_torque   = torque
        